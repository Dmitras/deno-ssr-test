// @deno-types="https://deno.land/x/types/react/v16.13.1/react.d.ts"
import React from "https://cdn.pika.dev/@pika/react@v16.13.1";
import ReactDOMServer from "https://dev.jspm.io/react-dom@16.13.1/server.js";

const Todos = () => {
    <div>
        <ul>
            <li>
                Clean flat
            </li>
            <li>
                Wash clothes
            </li>
            <li>
                Make dinner
            </li>
        </ul>
    </div>
}

export const page = (props: {}) => {
    retrun `<!DOCTYPE html>
    ${ReactDOMServer.renderToString((
        <Todos />
    ))}
`
}